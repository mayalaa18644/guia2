(function() {
    //var formulario = document.getElementById(id)
    var formulario = document.getElementsByName('formulario')[0];
        elementos = formulario.elements,
        boton = document.getElementById('btn');

    var validarNombre = function(e){
        if(formulario.nombre.value == 0) {
            alert("Por favor digite un nombre")
            e.preventdefault();
        }
    };
    
    var validarEmail = function(e){
        if(formulario.email.value == 0) {
            alert("Por favor digite su Email")
            e.preventdefault();
        }
    };

    var validarMensaje = function(e){
        if(formulario.mensaje.value == 0) {
            alert("Nos gustaría tener tu opinion")
            e.preventdefault();
        }
    };

    var validarEnviar = function(e){
        if(formulario.btn.value != 0) {
            alert("Sus datos fueron enviados con éxito")
            document.getElementByName("formulario").reset();
            e.preventdefault();
        }
    };

    var validar = function(e){
        validarNombre(e);
        validarEmail(e);
        validarMensaje(e);
        validarEnviar(e);
    };    
    
    formulario.addEventListener("submit", validar);    

}())
